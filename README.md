This page is to be used in addition to the [spree api documentation](http://guides.spreecommerce.org/api/) and contains instructions specific to using the api with curl

## Products
### List products
`curl -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/products`
### Show Product
The product slug is used to refer to individual products. 

`curl -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/products/PRODUCT_SLUG`

### Create Product
-X specified the HTTP method to use 
-d specifies the parameters to send
format your parameters like "product[PARAMETER]=VALUE"

`curl -X POST -d "product[name]=Headphones&product[price]=100&product[shipping_category_id]=1" -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/products`

### Update Product
 
`curl -X PUT -d "product[price]=10" -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/products/PRODUCT_SLUG`

### Search Products
Use -g (short for glob off) which gets curl to treat the [] in this url normally

`curl -g -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/products?q[name_cont]=test`

### Delete Product

`curl -X DELETE -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/products/PRODUCT_SLUG`

## Option types

### List Option Types

`curl -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/option_types`

### Create Option Type

`curl -X POST -d "option_type[name]=ot&option_type[presentation]=OT" -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/option_types`

### Delete Option Type

`curl -X DELETE -H "X-Spree-Token:  YOUR_TOEKEN" http://localhost:3000/api/v1/option_types/OPTION_TYPE_ID`

## Option Values

### List Option Values

`curl -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/option_types/OPTION_TYPE_ID/option_values`

### Create Option Value 

`curl -X POST -d "option_value[name]=test&option_value[presentation]=test" -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/option_types/OPTION_TYPE_ID/option_values`

### Delete Option Value

`curl -X DELETE -H "X-Spree-Token:  YOUR_TOKEN" http://localhost:3000/api/v1/option_types/OPTION_TYPE_ID/option_values/OPTION_VALUE_ID`